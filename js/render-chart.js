$(document).ready(function () {

  var myChart = new Highcharts.Chart('container', {
    chart: {
      type: highcharts_module.chart_type
    },
    title: {
      text: highcharts_module.label
    },
    xAxis: {
      categories: highcharts_module.labels
    },
    yAxis: {
      title: {
        text: highcharts_module.yTitle
      }
    },
    series: [{
      name: highcharts_module.xTitle,
      data: highcharts_module.values
    }]
  });
});
