<?php

/**
 * @file
 * Definition of Drupal\aschart\Plugin\views\style\AsChart.
 */

namespace Drupal\aschart\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render a list numbers as a chart using the as chart javascript.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "aschart",
 *   title = @Translation("As Chart"),
 *   help = @Translation("Renders highcharts chart on a drupal 8"),
 *   theme = "views_view_aschart",
 *   display_types = { "normal" }
 * )
 *
 */
class AsChart extends StylePluginBase {

    protected $usesFields = TRUE;

    /**
     * Does this Style plugin allow Row plugins?
     *
     * @var bool
     */
    protected $usesRowPlugin = TRUE;

    /**
     * Set default options
     */
    protected function defineOptions() {
        $options = parent::defineOptions();
        $options['path'] = array('default' => 'aschart');
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        parent::buildOptionsForm($form, $form_state);

        $form['chart_value'] = array(
            '#type' => 'textfield',
            '#title' => t('Chart Value Field Name'),
            '#default_value' => (isset($this->options['chart_value'])) ? $this->options['chart_value'] : 'chart_value',
            '#description' => t('High Charts Value Field'),
        );

        $form['chart_label_field'] = array(
            '#type' => 'textfield',
            '#title' => t('Chart Label Field'),
            '#default_value' => (isset($this->options['chart_label_field'])) ? $this->options['chart_label_field'] : '',
            '#description' => t('Chart label field. This will later be changed to a dropdown.'),
        );

        $options = array(
            'bar' => 'bar',
            'pie' => 'pie',
            'line' => 'line',
            'area' => 'area',
            'scatter' => 'scatter',
        );
        $form['chart_type'] = array(
            '#type' => 'radios',
            '#title' => t('Chart Type'),
            '#options' => $options,
            '#default_value' => (isset($this->options['chart_type'])) ? $this->options['chart_type'] : null,
            '#description' => t('Chart Type')
        );

        $form['classes'] = array(
            '#type' => 'textfield',
            '#title' => t('CSS classes'),
            '#default_value' => (isset($this->options['classes'])) ? $this->options['classes'] : 'view-aschart',
            '#description' => t('CSS classes for further customization of this highcharts page.'),
        );
    }


}
